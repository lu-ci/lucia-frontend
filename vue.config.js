module.exports = {
  chainWebpack: config => {
    // do not pointlessly copy `public` folder to `dist`
    // when not building for production
    if (process.env.NODE_ENV !== 'production') {
      config.plugins.delete('copy')
    }
  }
}
