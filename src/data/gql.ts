import { GraphQLClient } from 'graphql-request'
import LCValidator from './validation'

import md5 from 'js-md5'

const lcv = new LCValidator()

class LuciaCoreGQL {
  location = `${process.env.VUE_APP_CORE_API_URL}/graphql`

  request<T> (query: string, variables = {}): Promise<T> {
    const client = new GraphQLClient(this.location, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + lcv.makeKey()
      }
    })
    return new Promise((resolve, reject) => {
      client.request(query, variables)
        .then((data) => {
          this.save(query, JSON.stringify(data))
          resolve(data)
        })
        .catch((reason) => {
          const data = this.yoink(query) as T
          if (data) {
            resolve(data)
          } else {
            reject(reason)
          }
        })
    })
  }

  yoink<T> (query: string): T|null {
    const key = md5(query)
    const data = window.localStorage.getItem(key)
    if (data) {
      return JSON.parse(data) as T
    } else {
      return null
    }
  }

  save (query: string, data: string) {
    const key = md5(query)
    window.localStorage.setItem(key, data)
  }
}

export default LuciaCoreGQL
