import images from '@/data/images'

interface Icons {
  [index: string]: string
}

const icons: Icons = {
  lucia: '/img/lc_icon_square.png',
  sigma: images.projects.sigma.icon,
  javelin: images.projects.javelin.icon
}

export default icons
