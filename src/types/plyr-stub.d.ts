declare module 'plyr' {
  export type Target = NodeList | HTMLElement | HTMLElement[] | string;

  export interface Options {
    /**
    * Completely disable Plyr. This would allow you to do a User Agent check or similar to programmatically enable or disable Plyr for a certain UA. Example below.
    */
    enabled?: boolean;

    /**
    * Display debugging information in the console
    */
    debug?: boolean;

    /**
    * If a function is passed, it is assumed your method will return either an element or HTML string for the controls. Three arguments will be passed to your function; id (the unique id for the player), seektime (the seektime step in seconds), and title (the media title). See controls.md for more info on how the html needs to be structured.
    * Defaults to ['play-large', 'play', 'progress', 'current-time', 'mute', 'volume', 'captions', 'settings', 'pip', 'airplay', 'fullscreen']
    */
    controls?: string[] | Function | Element;

    /**
    * If you're using the default controls are used then you can specify which settings to show in the menu
    * Defaults to ['captions', 'quality', 'speed', 'loop']
    */
    settings?: string[];

    /**
    * Used for internationalization (i18n) of the text within the UI.
    */
    i18n?: any;

    /**
    * Load the SVG sprite specified as the iconUrl option (if a URL). If false, it is assumed you are handling sprite loading yourself.
    */
    loadSprite?: boolean;

    /**
    * Specify a URL or path to the SVG sprite. See the SVG section for more info.
    */
    iconUrl?: string;

    /**
    * Specify the id prefix for the icons used in the default controls (e.g. plyr-play would be plyr). This is to prevent clashes if you're using your own SVG sprite but with the default controls. Most people can ignore this option.
    */
    iconPrefix?: string;

    /**
    * Specify a URL or path to a blank video file used to properly cancel network requests.
    */
    blankUrl?: string;

    /**
    * Autoplay the media on load. This is generally advised against on UX grounds. It is also disabled by default in some browsers. If the autoplay attribute is present on a <video> or <audio> element, this will be automatically set to true.
    */
    autoplay?: boolean;

    /**
    * Only allow one player playing at once.
    */
    autopause?: boolean;

    /**
    * The time, in seconds, to seek when a user hits fast forward or rewind.
    */
    seekTime?: number;

    /**
    * A number, between 0 and 1, representing the initial volume of the player.
    */
    volume?: number;

    /**
    * Whether to start playback muted. If the muted attribute is present on a <video> or <audio> element, this will be automatically set to true.
    */
    muted?: boolean;

    /**
    * Click (or tap) of the video container will toggle play/pause.
    */
    clickToPlay?: boolean;

    /**
    * Disable right click menu on video to help as very primitive obfuscation to prevent downloads of content.
    */
    disableContextMenu?: boolean;

    /**
    * Hide video controls automatically after 2s of no mouse or focus movement, on control element blur (tab out), on playback start or entering fullscreen. As soon as the mouse is moved, a control element is focused or playback is paused, the controls reappear instantly.
    */
    hideControls?: boolean;

    /**
    * Reset the playback to the start once playback is complete.
    */
    resetOnEnd?: boolean;

    /**
    * Enable keyboard shortcuts for focused players only or globally
    */
    keyboard?: KeyboardOptions;

    /**
    * controls: Display control labels as tooltips on :hover & :focus (by default, the labels are screen reader only). seek: Display a seek tooltip to indicate on click where the media would seek to.
    */
    tooltips?: TooltipOptions;

    /**
    * Specify a custom duration for media.
    */
    duration?: number;

    /**
    * Displays the duration of the media on the metadataloaded event (on startup) in the current time display. This will only work if the preload attribute is not set to none (or is not set at all) and you choose not to display the duration (see controls option).
    */
    displayDuration?: boolean;

    /**
    * Display the current time as a countdown rather than an incremental counter.
    */
    invertTime?: boolean;

    /**
    * Allow users to click to toggle the above.
    */
    toggleInvert?: boolean;

    /**
    * Allows binding of event listeners to the controls before the default handlers. See the defaults.js for available listeners. If your handler prevents default on the event (event.preventDefault()), the default handler will not fire.
    */
    listeners?: Object;

    /**
    * active: Toggles if captions should be active by default. language: Sets the default language to load (if available). 'auto' uses the browser language. update: Listen to changes to tracks and update menu. This is needed for some streaming libraries, but can result in unselectable language options).
    */
    captions?: CaptionOptions;

    /**
    * enabled: Toggles whether fullscreen should be enabled. fallback: Allow fallback to a full-window solution. iosNative: whether to use native iOS fullscreen when entering fullscreen (no custom controls)
    */
    fullscreen?: FullScreenOptions;

    /**
    * The aspect ratio you want to use for embedded players.
    */
    ratio?: string;

    /**
    * enabled: Allow use of local storage to store user settings. key: The key name to use.
    */
    storage?: StorageOptions;

    /**
    * selected: The default speed for playback. options: Options to display in the menu. Most browsers will refuse to play slower than 0.5.
    */
    speed?: SpeedOptions;

    /**
    * Currently only supported by YouTube. default is the default quality level, determined by YouTube. options are the options to display.
    */
    quality?: QualityOptions;

    /**
    * active: Whether to loop the current video. If the loop attribute is present on a <video> or <audio> element, this will be automatically set to true This is an object to support future functionality.
    */
    loop?: LoopOptions;

    /**
    * enabled: Whether to enable vi.ai ads. publisherId: Your unique vi.ai publisher ID.
    */
    ads?: AdOptions;
  }

  export interface QualityOptions {
    default: string;
    options: string[];
  }

  export interface LoopOptions {
    active: boolean;
  }

  export interface AdOptions {
    enabled: boolean;
    publisherId: string;
  }

  export interface SpeedOptions {
    selected: number;
    options: number[];
  }

  export interface KeyboardOptions {
    focused?: boolean;
    global?: boolean;
  }

  export interface TooltipOptions {
    controls?: boolean;
    seek?: boolean;
  }

  export interface FullScreenOptions {
    enabled?: boolean;
    fallback?: boolean;
    allowAudio?: boolean;
  }

  export interface CaptionOptions {
    defaultActive?: boolean;
  }

  export interface StorageOptions {
    enabled?: boolean;
    key?: string;
  }

  class Plyr {
    constructor(targets: Target, options?: Options)
    static setup(targets: Target, options?: Options): Plyr[];
  }
  export default Plyr
}
